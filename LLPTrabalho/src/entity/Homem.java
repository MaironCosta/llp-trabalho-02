package entity;

import java.io.Serializable;

public final class Homem extends Pessoa implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Homem(String nome, int idade, Pessoa pai, Pessoa mae) {
		super(nome, idade, pai, mae);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Homem [toString()=" + super.toString() + "]";
	}	

}
