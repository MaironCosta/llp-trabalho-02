package entity;

import interfaces.ControleDePeso;

import java.io.Serializable;
import java.text.DecimalFormat;
//Teste
public final class Mulher extends Pessoa implements Serializable, ControleDePeso {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double peso;
	
	private double altura;

	public Mulher(String nome, int idade, Pessoa pai, Pessoa mae, double peso, double altura) {
		super(nome, idade, pai, mae);

		this.peso = peso;
		this.altura = altura;
	}
	
	public double getPeso() {
		return peso;
	}

	public double getAltura() {
		return altura;
	}

	public void calcularIMC () {
		
//		Abaixo de 17	Muito abaixo do peso
//		Entre 17 e 18,49	Abaixo do peso
//		Entre 18,5 e 24,99	Peso normal
//		Entre 25 e 29,99	Acima do peso
//		Entre 30 e 34,99	Obesidade I
//		Entre 35 e 39,99	Obesidade II (severa)
//		Acima de 40	Obesidade III (mórbida)
		
		double valorIMC = this.getPeso() / (Math.pow(this.getAltura(), 2)); 		
		DecimalFormat df = new DecimalFormat("##.##");
		
		System.out.println("IMC: " + df.format(valorIMC) + " - Situação: " + this.situacaoIMC(valorIMC));
		
	}

	private String situacaoIMC (double valorIMC) {
		
		if (valorIMC < 17d) {
			return "Muito abaixo do peso";
		} else if (valorIMC < 18.5d) {
			return "Abaixo do peso";
		} else if (valorIMC < 25d) {
			return "Peso Normal";
		} else if (valorIMC < 30d) {
			return "Acima do Peso";
		} else if (valorIMC < 35d) {
			return "Obesidade I";
		} else if (valorIMC < 40d) {
			return "Obesidade II (severa)";
		} else {
			return"Obesidade III (mórbida)";
		}
		
	}
		
	@Override
	public String toString() {
		return "Mulher [peso=" + peso + ", altura=" + altura + ", toString()="
				+ super.toString() + "]";
	}

	public static void main(String[] args) {
		
		ControleDePeso mulher = new Mulher(null, 0, null, null, 74d, 1.75d);
		
		mulher.calcularIMC();
		
	}
	
}
