package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

// POG T
public abstract class Pessoa implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nome;
	
	private int idade;
		
	private Pessoa conjugue;
	
	private List<Pessoa> exConjugue;
	
	private Pessoa pai;
	
	private Pessoa mae;
	
	public Pessoa(String nome, int idade, Pessoa pai, Pessoa mae) {
		super();
		this.nome = nome;
		this.idade = idade;
		this.pai = pai;
		this.mae = mae;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public Pessoa getConjugue() {
		return conjugue;
	}

	public void setConjugue(Pessoa conjugue) {
		this.conjugue = conjugue;
	}

	public List<Pessoa> getExConjugue() {
		return exConjugue;
	}

	public void setExConjugue(List<Pessoa> exConjugue) {
		this.exConjugue = exConjugue;
	}

	public Pessoa getPai() {
		return pai;
	}

	public void setPai(Pessoa pai) {
		this.pai = pai;
	}

	public Pessoa getMae() {
		return mae;
	}

	public void setMae(Pessoa mae) {
		this.mae = mae;
	}

	public void casar(Pessoa pessoa) {
		
		if (!this.validarCasamento(pessoa)) {
			return;
		}
		
		this.setConjugue(pessoa);
		pessoa.setConjugue(this);
		
		System.out.println("*** casados ***");
		
	}

	private boolean validarCasamento(Pessoa pessoa) {
		
		if (pessoa == null) {
			
			System.out.println("### n�o pode casar com pessoa nula");
			return false;
			
		} else if (this.getConjugue() != null) {
			
			System.out.println("### possui conjugue " + this);
			return false;
			
		} else if (pessoa.getConjugue() != null) {
			
			System.out.println("### possui conjugue " + pessoa);
			return false;
			
		} else if (this.getIdade() < 16 || pessoa.getIdade() < 16) {
			
			System.out.println("### idade inferior a 16 anos");
			return false;
			
		} else if (this.equals(pessoa)) {
			
			System.out.println("### n�o pode se auto relacionado");
			return false;
			
		}
		
		return true;
	}
	
	public void divorciar (Pessoa pessoa) {
		
		if (pessoa == null) {
			System.out.println("### pessoa não informada ###");
			return;
		}
		
		if (this.equals(pessoa)) {
			System.out.println("### são a mesma pessoa ###");
			return;
		}
		
		if (this.getConjugue() != null && pessoa.getConjugue() != null) {
			
			if (this.getConjugue().equals(pessoa) && pessoa.getConjugue().equals(this.getConjugue())) {
				
				this.getExConjugue().add(pessoa);
				pessoa.getExConjugue().add(this);
				
				this.setConjugue(null);
				pessoa.setConjugue(null);
				
				System.out.println("*** Divórcio efetuado! ***");
				
			} else {
				
				System.out.println("### Não são casados. ###");
			}
			
		} else if (this.getConjugue() != null && pessoa.getConjugue() == null) {
			
			System.out.println(this.getNome() + " é casado com " + this.getConjugue().getNome());			
			System.out.println(" e " + pessoa.getConjugue().getNome() + " não é casado.");
			
		} else if (this.getConjugue() == null && pessoa.getConjugue() != null) {
			
			System.out.println(pessoa.getConjugue() + " é casado com " + pessoa.getConjugue().getConjugue().getNome());			
			System.out.println(" e " + this.getNome() + " não é casado.");
			
		}
		
	}
	
	public void listarFilhos (Collection<Pessoa> pessoas) {

		for (Pessoa p : pessoas) {

			if (this.getPai() != null && this.getPai().equals(p)) {

				System.out.println("Nome pai: " + p.getNome());
				
			} 
			
			if (this.getMae() != null && this.getMae().equals(p)) {

				System.out.println("Nome mae: " + p.getNome());
				
			}

		}
		
	}
	
	public void verificarParentesco (Pessoa pessoa) {
		
		if (pessoa == null) {
			System.out.println("Pessoa não informada");
			return;
		}
		
		if (pessoa.equals(this.getPai())) {
			System.out.println(this.getNome() + " é pai de " + pessoa.getNome());
			return;
		} else if (pessoa.equals(this.getMae())) {
			System.out.println(this.getNome() + " é mãe de " + pessoa.getNome());
			return;
		} else if (pessoa.equals(this.getPai().getPai()) || pessoa.equals(this.getMae().getPai())) {
			System.out.println(this.getNome() + " é avô de " + pessoa.getNome());
			return;
		} else if (pessoa.equals(this.getPai().getMae()) || pessoa.equals(this.getMae().getMae())) {
			System.out.println(this.getNome() + " é avó de " + pessoa.getNome());
			return;
		}		
	}
	
	public void listarCasamento () {
		
		System.out.println("Lista Casamentos de " + this.getNome());
		System.out.println((this.getConjugue() != null)?"Casamento Atual \n Casado com " + this.getConjugue().getNome(): "N�o � casado!");
		
		if (!this.getExConjugue().isEmpty()) {
			
			System.out.println("Casamentos passados");
			
			for (Pessoa p : this.getExConjugue()) {
				
				System.out.println("### foi casado com: " + p.getNome());
				
			}
			
		}
		
	}
	
	public void verificarEstadoCivil () {
		
		if (this.getConjugue() == null && this.getExConjugue().isEmpty()) {
			
			System.out.println("*** " + this.getNome() + " � solteiro ******");
			
		} else if (this.getConjugue() != null) {
			
			System.out.println(" ##### " + this.getNome() + " � casado ######");
			
		} else if (this.getConjugue() == null && !this.getExConjugue().isEmpty()) {
			
			System.out.println("*** " + this.getNome() + " � divorciado ******");
			
		}
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((conjugue == null) ? 0 : conjugue.hashCode());
		result = prime * result
				+ ((exConjugue == null) ? 0 : exConjugue.hashCode());
		result = prime * result + idade;
		result = prime * result + ((mae == null) ? 0 : mae.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((pai == null) ? 0 : pai.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (conjugue == null) {
			if (other.conjugue != null)
				return false;
		} else if (!conjugue.equals(other.conjugue))
			return false;
		if (exConjugue == null) {
			if (other.exConjugue != null)
				return false;
		} else if (!exConjugue.equals(other.exConjugue))
			return false;
		if (idade != other.idade)
			return false;
		if (mae == null) {
			if (other.mae != null)
				return false;
		} else if (!mae.equals(other.mae))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (pai == null) {
			if (other.pai != null)
				return false;
		} else if (!pai.equals(other.pai))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", idade=" + idade + ", conjugue="
				+ conjugue + ", exConjugue=" + exConjugue + ", pai=" + pai
				+ ", mae=" + mae + "]";
	}
	
}
