package testeArvore;

import java.util.ArrayList;
import java.util.List;

import entity.Homem;
import entity.Mulher;
import entity.Pessoa;

public class TesteArvore {
	
	private static List<Pessoa> pessoas = new ArrayList<>();
		
	public static List<Pessoa> getPessoas() {
		return pessoas;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
		Pessoa homem = new Homem("Teste 01", 20, null, null);
		TesteArvore.getPessoas().add(homem);
		
		Pessoa mullher = new Mulher("Teste 01", 20, null, null, 60d, 1.75d);
		TesteArvore.getPessoas().add(mullher);
		
		for (Pessoa p : TesteArvore.getPessoas()) {
			System.out.println(p);
		}
		
	}

}
